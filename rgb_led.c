/* Dooba SDK
 * RGB LED Control
 */

// External Includes
#include <stdlib.h>
#include <avr/io.h>
#include <pwm/pwm.h>

// Internal Includes
#include "rgb_led.h"

// Set RGB LED
void rgb_led(uint8_t r_pin, uint8_t g_pin, uint8_t b_pin, uint8_t r, uint8_t g, uint8_t b)
{
	pwm_set(r_pin, r);
	pwm_set(g_pin, g);
	pwm_set(b_pin, b);
}
