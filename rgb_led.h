/* Dooba SDK
 * RGB LED Control
 */

#ifndef	__RGB_LED_H
#define	__RGB_LED_H

// External Includes
#include <stdint.h>

// Set RGB LED
extern void rgb_led(uint8_t r_pin, uint8_t g_pin, uint8_t b_pin, uint8_t r, uint8_t g, uint8_t b);

#endif
